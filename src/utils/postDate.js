export const postDate = date => {
  const dateString = new Date(date);

  const weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  const timeMinutes = dateString.getMinutes();

  const dateTime = {
    hours:
      dateString.getHours() === 0
        ? "12"
        : dateString.getHours() < 12
        ? dateString.getHours()
        : dateString.getHours() - 12,
    minutes:
      dateString.getMinutes() < 10
        ? "0".concat(timeMinutes)
        : dateString.getMinutes(),
    ampm: dateString.getHours() < 12 ? "AM" : "PM"
  };

  return `${weekdays[dateString.getDay()]}, ${
    months[dateString.getMonth()]
  } ${dateString.getDate()}, ${dateString.getFullYear()} at ${dateTime.hours}:${
    dateTime.minutes
  } ${dateTime.ampm}`;
};
