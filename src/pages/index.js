import React from "react";
import { Link, graphql } from "gatsby";
import Img from "gatsby-image";
import styled from "styled-components";

import Bio from "../components/bio";
import Layout from "../components/layout";
import SEO from "../components/seo";
import { rhythm } from "../utils/typography";
import { postDate } from "../utils/postDate";

import "../styles/index.scss";

const Post = styled.div`
  display: flex;
  border-bottom: 1px solid #000;
  margin-bottom: 3rem;
  padding-bottom: 1rem;
`;
const PostImage = styled.div`
  flex: 25%;
  margin-right: 1rem;
`;
const PostText = styled.div`
  flex: 75%;
`;

class BlogIndex extends React.Component {
  render() {
    const { data } = this.props;
    const siteTitle = data.site.siteMetadata.title;
    const posts = data.allContentfulPost.edges;

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO
          title="All posts"
          keywords={[`blog`, `gatsby`, `javascript`, `react`]}
        />
        <Bio />
        {posts.map(({ node }) => {
          const title = node.title || node.slug;
          const thisPostDate = postDate(node.createdAt);

          let updatedDate;
          if (node.createdAt !== node.updatedAt) {
            updatedDate = `Updated on ${postDate(node.updatedAt)}`;
          }

          return (
            <Post key={node.slug}>
              <PostImage>
                <Img fluid={node.image.fluid} />
              </PostImage>
              <PostText>
                <h3
                  style={{
                    marginTop: 0,
                    marginBottom: rhythm(1 / 4)
                  }}
                >
                  <Link style={{ boxShadow: `none` }} to={node.slug}>
                    {title}
                  </Link>
                </h3>
                <small>
                  Posted on {thisPostDate} by {node.author}
                  <span className="updatedDate">{updatedDate}</span>
                </small>
                <p
                  className="postDescription"
                  dangerouslySetInnerHTML={{ __html: node.subtitle }}
                />
              </PostText>
            </Post>
          );
        })}
      </Layout>
    );
  }
}

export default BlogIndex;

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allContentfulPost {
      edges {
        node {
          title
          subtitle
          author
          createdAt
          updatedAt
          image {
            fluid {
              ...GatsbyContentfulFluid
            }
          }
          slug
        }
      }
    }
  }
`;
